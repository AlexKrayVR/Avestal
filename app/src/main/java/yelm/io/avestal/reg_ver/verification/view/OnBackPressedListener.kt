package yelm.io.avestal.reg_ver.verification.view

interface OnBackPressedListener {
    fun doBack()
}